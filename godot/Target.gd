extends StaticBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func dim():
	var tween =  Tween.new()
	add_child(tween)
	var anitime = 1
	tween.interpolate_property(self,"modulate",Color(1,1,1,1),Color8(200,200,200,255),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween.start()

func retreat():
	var tw = Tween.new()
	var target = $"../initial_target_position".position
	tw.interpolate_property(self,"position",position,target,1.0,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tw.start()
	add_child(tw)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
