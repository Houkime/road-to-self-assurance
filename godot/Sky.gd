extends Polygon2D

var clouds = []
var cloud_spawn_frequency = 2.0

var brightness = 240
var windspeed = 20
var spawnbox_width = 100
var spawnbox_height = 200
var spawnbox_min_x = 1024+40
var max_size = 3
var timer
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.

func spawn_cloud():
	var cloud = Cloud.new(brightness,windspeed,max_size)
	var pos = Vector2(rand_range(spawnbox_min_x,spawnbox_min_x+spawnbox_width),rand_range(0,spawnbox_height))
	cloud.position = pos
	add_child(cloud)
	
func gray():
	var tween1 =  Tween.new()
	add_child(tween1)
	var anitime = 1
	tween1.interpolate_property(self,"material:shader_param/color1",material.get_shader_param("color1"),Color8(180,180,180,255),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween1.start()
	var tween2 =  Tween.new()
	add_child(tween2)
	tween2.interpolate_property(self,"material:shader_param/color2",material.get_shader_param("color2"),Color8(150,150,150,255),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween2.start()
	var tween3 =  Tween.new()
	add_child(tween3)
	tween3.interpolate_property(self,"material:shader_param/color3",material.get_shader_param("color3"),Color8(100,100,100,255),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween3.start()

func ardent():
	var tween1 =  Tween.new()
	add_child(tween1)
	var anitime = 1
	tween1.interpolate_property(self,"material:shader_param/color1",material.get_shader_param("color1"),Color8(180,180,0,255),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween1.start()
	var tween2 =  Tween.new()
	add_child(tween2)
	tween2.interpolate_property(self,"material:shader_param/color2",material.get_shader_param("color2"),Color8(100,30,30,255),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween2.start()
	var tween3 =  Tween.new()
	add_child(tween3)
	tween3.interpolate_property(self,"material:shader_param/color3",material.get_shader_param("color3"),Color8(50,40,40,255),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween3.start()

func _ready():
	#ardent()
	timer = Timer.new()
	timer.wait_time = 1.0/cloud_spawn_frequency
	timer.connect("timeout",self,"spawn_cloud")
	add_child(timer)
	#timer.start()
	
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
