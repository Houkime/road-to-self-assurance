extends Polygon2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.

func dim():
	var tween =  Tween.new()
	add_child(tween)
	var anitime = 1
	tween.interpolate_property(self,"modulate",Color(1,1,1,1),Color8(170,170,170,255),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween.start()

func black():
	var tween =  Tween.new()
	add_child(tween)
	var anitime = 1
	tween.interpolate_property(self,"color",color,Color8(80,80,80,255),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween.start()

func _ready():
	#dim()
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
