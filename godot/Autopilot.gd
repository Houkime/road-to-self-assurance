extends Node2D

onready var bow = $"../Bow"

# Called when the node enters the scene tree for the first time.

func on_timeout():
	if bow.active:
		queue_free()
	else:
		bow.stretching = !bow.stretching

func _ready():
	bow.active = false
	var timer = Timer.new()
	timer.wait_time = 1
	timer.connect("timeout",self,'on_timeout')
	add_child(timer)
	timer.start()
	