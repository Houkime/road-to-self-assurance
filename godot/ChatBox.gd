extends VBoxContainer

var current_texts = null
var col = ColorN("yellow")

var texts = {
	"initial": [
	"This man can't shoot",
	"Mommy, let's go, it is boring!",
	"He doesn't seem to try",
	"BOOOORIIING!",
	"WOOOO",
	"Even I can shoot better!"
	],
	"startlevel": [
	"Hey, try to be better that him!",
	"We're counting on you!",
	"Hmmm",
	"Try more!",
	"At least you try"
	],
	"wind": [
	"The Archery Fest is already over!",
	"Why are you still here?!",
	"It's cold and frosty, come to bar with us!",
	"What's wrong with you?",
	"Well... if you want to continue shooting..."
	],
	"building": [
	"We have bought this land!",
	"Go away!",
	"We will call the police!",
	"We will sue you!",
	"You will pay for that!",
	"Previous owner won't help you, he is found dead",
	"Bribes cost us money and nerves, you know?",
	"Giant bureaucratic building is better than some archery range!"
	],
	"police": [
	"NEW LAW!! BOWS ARE FORBIDDEN!!",
	"You can't have a bow anymore!",
	"*frrr* TRYING TO DETAIN A POTENTIAL TERRORIST",
	"So called citizens don't need weapons!",
	"*frr* DROP THE BOW AND COME TO US WITH YOUR HANDS UP!!",
	"*frr* YOU THINK YOU GOT RIGHTS PAL?",
	"PRAISE THE BALD KING YOU SCUM"
	],
	"king": [
	"MWAHHAHAHAHAHA",
	"YOU CANNOT HAVE FREEDOM TO SHOOT AT RANGE!",
	"YOU CANNOT HAVE ANY FREEDOMS AT ALL",
	"GO WORK!",
	"WORK AND PAY TAXES!!!",
	"I AM INVINSIBLE AND GOD IS ON MY SIDE",
	"I WILL GO TO HEAVEN AND YOU JUST DIE",
	"I DONT CARE THAT THERE ONLY RUINS REMAINING!",
	"I DONT CARE THAT HALF OF POPULATION DEAD AS LONG AS I AM THE KING!",
	"YOU NEED TO HONOR ME SCUM!"
	],
	"king_last": [
	"HOUKIME YOU BETRAYED ME!",
	"HOUKIME SAID I WOULD BE INVINCIBLE!!",
	"HOUKIME SAID I WILL BE AN ARCH NEMESIS!",
	"DONT POINT THE BOW AT ME!",
	"DONT YOU DARE!",
	"THAT'S NOT FAIR!"
	],
}

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	setup_texts("initial")
	var timer = Timer.new()
	timer.wait_time = 2
	add_child(timer)
	timer.connect("timeout",self,"speak")
	timer.start()
	randomize()
	speak()

func speak():
	var text  = current_texts[randi() % current_texts.size()]
	add_message(text)
	
func setup_texts(setname):
	current_texts = texts[setname]

func add_message(text):
	add_child(ChatMessage.new(text,col))