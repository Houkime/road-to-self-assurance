extends Node2D

var wind_spawn_frequency = 4.0

var brightness = 240
var min_wind = 300
var max_wind = 650
var max_period = 2.0
var min_period = 0.3
var windspeed = 400
var spawnbox_width = 100
var spawnbox_height = 600
var spawnbox_min_x = 1024+40
var active = false
var timer
var anitimer
var anitw

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func spawn_wind():
	var wind = Wind.new(brightness,windspeed)
	var pos = Vector2(rand_range(spawnbox_min_x,spawnbox_min_x+spawnbox_width),rand_range(0,spawnbox_height))
	wind.position = pos
	add_child(wind)

func change_wind():
	if anitimer!=null:
		anitimer.queue_free()
	if anitw!=null:
		anitw.queue_free()
	var target = rand_range(min_wind,max_wind)
	var time = rand_range(min_period,max_period)
	#print("changing wind to ", target)
	anitimer = Timer.new()
	anitimer.wait_time = time
	add_child(anitimer)
	anitimer.start()
	anitimer.connect("timeout",self,"change_wind")
	anitw = Tween.new()
	anitw.interpolate_property(self,"windspeed",windspeed,target,time,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	add_child(anitw)
	anitw.start()
	

func _ready():
	randomize()
	timer = Timer.new()
	timer.wait_time = 1.0/wind_spawn_frequency
	timer.connect("timeout",self,"spawn_wind")
	add_child(timer)
	#timer.start()