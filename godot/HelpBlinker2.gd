extends Label

var waiting = true
var timer

func on_timeout():
	self.visible = !self.visible

func _process(delta):
	if waiting:
		if Input.is_action_just_pressed("ui_accept"):
			var winder = $"../../Winder"
			winder.min_wind = 0
			winder.max_wind = 0
			queue_free()

func activate():
	timer.start()
	waiting = true

func _ready():
	timer = Timer.new()
	timer.wait_time = 1
	timer.connect("timeout",self,'on_timeout')
	text = "PRESS X TO STOP WIND BECAUSE THE TIME HAS CHANGED"
	add_child(timer)
	