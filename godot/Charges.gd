extends Label

var hits = 0
var separation = 50.0

func roll_in():
	var tween =  Tween.new()
	add_child(tween)
	var anitime = 1
	var pos= rect_position
	rect_position = $initpos.global_position
	tween.interpolate_property(self,"rect_position",rect_position,pos+Vector2(separation,0.0),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween.start()
	
	var tween2 =  Tween.new()
	add_child(tween2)
	var pos2= $"../HitStat".rect_position
	tween2.interpolate_property($"../HitStat","rect_position",pos2,pos2+Vector2(-separation,0.0),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween2.start()
	
	visible = true
	update_text()
	
func update_text():
	text = "CRIMINAL CHARGES: "+ str(hits)

func add_hit():
	hits+=1
	update_text()
