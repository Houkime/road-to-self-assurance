extends TextureRect

var label
var anitime = 1
var th = load("res://theme.tres")

var lifetime = 5

class_name ChatMessage

func _init(text,col):
	label = Label.new()
	label.theme = th
	rect_min_size = Vector2(50,10)
	label.add_color_override("font_color", col)
	label.text = text
	#visible = false
	add_child(label)

func _ready():
	roll_in()

func roll_in():
	var tween =  Tween.new()
	add_child(tween)
	var pos= rect_position
	label.rect_position = Vector2(-300.0,0.0)
	tween.interpolate_property(label,"rect_position",label.rect_position,Vector2(0.0,0.0),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween.start()
	visible = true
	var timer = Timer.new()
	timer.wait_time=lifetime
	timer.start()
	timer.one_shot = true
	add_child(timer)
	timer.connect("timeout",self,"cleanup")

func cleanup():
	queue_free()
	pass
	
	pass # Replace with function body.