extends Polygon2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	color = find_parent("Main").find_node("Ground").color
	connect("visibility_changed",self,"colorize")
	pass # Replace with function body.

func colorize():
	color = find_parent("Main").find_node("Ground").color
	modulate = find_parent("Main").find_node("Ground").modulate

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
