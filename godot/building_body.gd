extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	#roll_in()
	#$StaticBody2D/CollisionPolygon2D.disabled = false
	pass # Replace with function body.

func roll_in():
	var tween =  Tween.new()
	add_child(tween)
	var anitime = 1
	var pos= position
	find_parent("Main").start_shake(anitime)
	position = $initpos.global_position-get_parent().global_position
	tween.interpolate_property(self,"position", position,pos,anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween.start()
	visible = true
	find_node("CollisionPolygon2D").set_deferred("disabled", false)
	print ("disabled? ", find_node("CollisionPolygon2D").disabled)

func retreat():
	var tween =  Tween.new()
	add_child(tween)
	var anitime = 1
	find_parent("Main").start_shake(anitime)
	var pos = $initpos.global_position-get_parent().global_position
	tween.interpolate_property(self,"position", position,pos,anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween.start()
	var timer = Timer.new()
	timer.wait_time=anitime
	timer.start()
	add_child(timer)
	timer.connect("timeout",self,"cleanup")

func cleanup():
	get_parent().queue_free()
	$"../../Ground".black()
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
