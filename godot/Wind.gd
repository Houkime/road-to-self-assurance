extends Line2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

class_name Wind

var speed = 0
var speed_dispersion = 1.0
var base_size = 10.0

# Called when the node enters the scene tree for the first time.
func _init(average_brightness, average_speed, max_size = 3):
	randomize()
	var base_vector = Vector2(1.0,0.0)*base_size
	points = [-base_vector,base_vector]
	width = 1.0
	scale = Vector2(2.0,2.0)
	var brightness = average_brightness + (randi() % 50) - 25
	brightness = confine(brightness, 0, 255)
	var col = Color8(brightness,brightness,brightness)
	modulate = col
	z_index = 1
	default_color = ColorN("white")
	speed = average_speed + rand_range(0,average_speed*speed_dispersion)
	speed = - speed
	scale = Vector2 (rand_range(0.8,max_size),rand_range(0.8,max_size))

func _process(delta):
	position+=Vector2(speed*delta,0)
	#if position.x<-40: position+=Vector2(1024+40,0)
	if position.x<-40: queue_free()
	

func confine(value, minimum,  maximum):
	if value > maximum: value = maximum
	if value < minimum: value = minimum
	return value

func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
