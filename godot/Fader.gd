extends Polygon2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	visible = true
	var tween =  Tween.new()
	add_child(tween)
	var anitime = 1
	tween.interpolate_property(self,"modulate",Color(1,1,1,1),Color(1,1,1,0),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween.start()