extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var jump_period = 3.0
export var jump_delay = 1.0
export var jump_speed = 700

var speed = Vector2(0.0,0.0)
var gravity = Vector2(0,9.8*60)
onready var basepos = position
onready var timer = Timer.new()
onready var jumptimer = Timer.new()

class_name Policeman
# Called when the node enters the scene tree for the first time.

func activate():
	timer.start()
	$Policeman/CollisionPolygon2D.set_deferred("disabled", false)

func start_jumping():
	jumptimer.start()
	jumptimer.connect("timeout",self,"jump")
	jump()

func jump():
	speed+=Vector2(0,-jump_speed)

func _process(delta):
		position+=speed*delta
		speed+=gravity*delta
		if position.y>basepos.y:
			position = basepos
			speed = Vector2(0.0,0.0)

func _ready():
	add_child(timer)
	add_child(jumptimer)
	timer.wait_time = jump_delay
	timer.one_shot = true
	jumptimer.wait_time = jump_period
	timer.connect("timeout",self, "start_jumping")