extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	find_node("Node2D").visible = true
	pass # Replace with function body.

func retreat():
	var tw = Tween.new()
	var target = $retreat.position
	var pos = position
	var timer = Timer.new()
	timer.wait_time=1
	timer.start()
	add_child(timer)
	timer.connect("timeout",self,"queue_free")
	tw.interpolate_property($AnimationPlayer/Node2D,"position",$AnimationPlayer/Node2D.position,target,1.0,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tw.start()
	add_child(tw)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
