extends Polygon2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	visible = false

func _fade_in():
	var tween =  Tween.new()
	add_child(tween)
	var anitime = 1
	modulate = Color(1,1,1,0)
	tween.interpolate_property(self,"modulate",Color(1,1,1,0),Color(1,1,1,1),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween.start()
	visible = true