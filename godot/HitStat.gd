extends Label

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var hits = 0
var needed = 6
var current_action = -1
var won = false


var actions = [
	"call_the_wind",
	"building",
	"police",
	"king"
]

# Called when the node enters the scene tree for the first time.
func _ready():
	set_ui_color(ColorN("yellow"))
	pass
	
func roll_in():
	var tween =  Tween.new()
	add_child(tween)
	var anitime = 1
	var pos= rect_position
	rect_position = $initpos.global_position
	tween.interpolate_property(self,"rect_position",rect_position,pos,anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween.start()
	visible = true
	update_text()

func update_text():
	text = "HITS: "+ str(hits)+"/"+str(needed)

func add_hit():
	hits+=1
	update_text()
	if hits == needed:
		current_action+=1
		if current_action<actions.size():
			call(actions[current_action])
		hits = 0
		update_text()


func call_the_wind():
	print ("calling the wind")
	var winder = $"../../Winder"
	winder.active = true
	winder.timer.start()
	winder.change_wind()
	$"../../SkyAnim/Sky".windspeed*=2
	$"../../SkyAnim/Sky".max_size=6
	$"../../SkyAnim/Sky".brightness=200
	$"../../Ground".dim()
	$"../../Target".dim()
	$"../ChatBox".setup_texts("wind")

func building():
	print ("summoning_building")
	var winder = $"../../Winder"
	winder.min_wind = 50
	winder.max_wind = 75
	$"../../SkyAnim/Sky".windspeed/=2
	$"../../SkyAnim/Sky".max_size=8
	$"../../SkyAnim/Sky".brightness=190
	$"../../SkyAnim/Sky".gray()
	$"../../Building".visible = true
	$"../../Building/body".roll_in()
	$"../ChatBox".setup_texts("building")
	$"../Charges".roll_in()
	set_ui_color(ColorN("cyan"))

func police():
	print ("calling police")
	var winder = $"../../Winder"
	winder.min_wind = 50
	winder.max_wind = 75
	$"../../SkyAnim/Sky".windspeed*=2
	$"../../SkyAnim/Sky".max_size=10
	$"../../SkyAnim/Sky".brightness=180
	$"../../Building/body".retreat()
	$"../../Police".roll_in()
	$"../ChatBox".setup_texts("police")
	hits = 0
	
func king():
	print ("summoning King")
	var winder = $"../../Winder"
	winder.min_wind = 400
	winder.max_wind = 800
	$"../../SkyAnim/Sky".windspeed*=2
	$"../../SkyAnim/Sky".max_size=12
	$"../../SkyAnim/Sky".brightness=120
	$"../../SkyAnim/Sky".ardent()
	$"../../Bow".dim()
	$"../../King".visible = true
	$"../../King/body".roll_in()
	$"../../Police".retreat()
	var treasontimer = Timer.new()
	treasontimer.wait_time=30
	treasontimer.connect("timeout",self,'treason')
	treasontimer.start()
	treasontimer.one_shot = true
	call_deferred("add_child",treasontimer)
	$"../ChatBox".setup_texts("king")
	set_ui_color(ColorN("white"))
	
func treason():
	print ("dropping king under a bus")
	$"../HelpBlinker2".activate()
	$"../ChatBox".setup_texts("king_last")
	$"../ChatBox".add_message("WAIT! WHAT'S THAT WHITE THING AT THE BOTTOM?")

func set_ui_color(col):
	$"../ChatBox".col = col
	add_color_override("font_color",col)
	$"../Charges".add_color_override("font_color",col)
func win():
	if !won:
		print ("YOU WON")
		$"../../OutFader"._fade_in()
		won = true