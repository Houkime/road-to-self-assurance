extends Label

var waiting = true
var intro_done = false
var timer
var counts = 0

onready var bow = $"../../Bow"

func on_timeout():
	self.visible = !self.visible

func _process(delta):
	if waiting:
		if Input.is_action_just_pressed("ui_select"):
			#visible = false
			#timer.stop()
			bow.stretching = false
			bow.active = true
			text= "SPACE TO FIRE"
			counts+=1
			if !intro_done:
				$"../../Crowd".retreat()
				$"../../Crowd2".retreat()
				$"../../Bow".retreat()
				$"../../Target".retreat()
				$"../../SkyAnim/Sky".timer.start()
				$"../..".hits.roll_in()
				$"../../Zshifter/ChatBox".setup_texts("startlevel")
				intro_done = true
	if counts>=5:
		queue_free()

func _ready():
	timer = Timer.new()
	timer.wait_time = 1
	timer.connect("timeout",self,'on_timeout')
	add_child(timer)
	timer.start()