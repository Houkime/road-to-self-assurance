extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
	
func roll_in():
	var tween =  Tween.new()
	add_child(tween)
	var anitime = 1
	var pos= position
	position = $initpos.global_position
	tween.interpolate_property(self,"position",position,pos,anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween.start()
	visible = true
	for c in get_children():
		if c is Policeman:
			c.activate()

func _ready():
	visible = false
	#roll_in()
	pass # Replace with function body.

func retreat():
	var tween =  Tween.new()
	add_child(tween)
	var anitime = 1
	var pos = $initpos.global_position
	tween.interpolate_property(self,"position", position,pos,anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween.start()
	var timer = Timer.new()
	timer.wait_time=anitime
	timer.start()
	add_child(timer)
	timer.connect("timeout",self,"queue_free")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
