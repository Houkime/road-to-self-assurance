extends Sprite

var extension=0.0
var max_extension=20.0
var jung = 2500
var arrow_mass = 1
var energy = 0
var string
var arrow
var arrow_size
var active = true
var stretching = false
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func _process(delta):
	if active:
		var angle = get_local_mouse_position().angle()
		rotate(angle)
	#if (Input.is_mouse_button_pressed(BUTTON_LEFT) and active) or stretching:
	if (Input.is_action_pressed("ui_select") and active) or stretching:
		if arrow == null:
			arrow = load("res://Arrow.tscn").instance()
			get_parent().add_child(arrow)
			arrow_size = arrow.find_node("Sprite").scale.x*10.0
			arrow.free_flying = false
		place_arrow()
		update_string()
		extension+=(max_extension - extension)*0.1
		#print(extension)
	else:
		if arrow != null:
			arrow.rotate(rand_range(-0.3,0.3)) #random deviations
			var energy = jung*extension*extension
			var arrow_speed = sqrt(energy/arrow_mass)
			arrow.speed = Vector2(cos(rotation),sin(rotation))*arrow_speed
			#print(arrow_speed)
			arrow.free_flying = true
			arrow = null
			extension=0.0
			update_string()
	

func place_arrow():
	arrow.rotation = rotation
	arrow.position = position + Vector2(cos(rotation),sin(rotation))*(4*scale.x-extension)

func update_string():
	var p = []
	p.append($elbow1.position)
	p.append(($elbow1.position+$elbow2.position)/2-Vector2(extension/4.0,0))
	p.append($elbow2.position)
	string.points = p
	
# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	string = Line2D.new()
	string.width = 0.3
	var p = []
	update_string()
	add_child(string)

func retreat():
	var tw = Tween.new()
	var target = $"../initial_bow_position".position
	tw.interpolate_property(self,"position",position,target,1.0,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tw.start()
	add_child(tw)

func dim():
	var tween =  Tween.new()
	add_child(tween)
	var anitime = 1
	tween.interpolate_property(self,"modulate",Color(1,1,1,1),Color8(150,120,120,255),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween.start()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
