extends KinematicBody2D

var speed = Vector2(700,0)
var gravity = Vector2(0,9.8*60)
var angle_bleed = 0.01
var free_flying = true
var started = false
var timer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.

func _process(delta):
	if free_flying == true:
		if !started:
			timer.start()
			started = true
		position+=speed*delta
		var target_angle = speed.angle()
		#print (target_angle)
		rotation+=(target_angle-rotation) * angle_bleed * speed.length()/100
		#print(rotation)
		var wind_accel = Vector2($"../Winder".windspeed*8,0)*(abs(rotation))/(PI/2.0) if $"../Winder".active else Vector2(0.0,0.0)
		#print(wind_accel)
		speed+=(gravity-wind_accel)*delta

func collided_with(body):
	if body.name == "Target":
		find_parent("Main").hits.add_hit()
	if body.name in ["StaticBody2D","Policeman"]:
		find_parent("Main").charges.add_hit()
	if body.name in ["Policeman"]:
		queue_free()
	if body.name == "KingCollider":
		find_parent("Main").hits.win()
	free_flying= false


func _ready():
	timer = Timer.new()
	timer.wait_time = 10
	timer.connect("timeout",self,'queue_free')
	add_child(timer)