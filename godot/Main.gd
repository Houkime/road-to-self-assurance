extends Node2D

onready var hits = find_node("HitStat")
onready var charges = find_node("Charges")

var anitimer
var anitw
var amplitude = 15
var min_period = 0.1
var max_period = 0.2
var stop_shaking = true
var shaketimer

func start_shake(time):
	if shaketimer!=null:
		shaketimer.queue_free()
	shaketimer = Timer.new()
	call_deferred("add_child",shaketimer)
	shaketimer.wait_time = time
	shaketimer.one_shot = true
	shaketimer.start()
	shaketimer.connect("timeout",self,"stopshake")
	stop_shaking = false
	shake()
	OS.get_latin_keyboard_variant()
	

func shake():
	if anitimer!=null:
		anitimer.queue_free()
	if anitw!=null:
		anitw.queue_free()
	var target = Vector2(rand_range(-amplitude,amplitude),rand_range(-amplitude,amplitude))
	var time = rand_range(min_period,max_period)
	#print("changing wind to ", target, stop_shaking)
	anitimer = Timer.new()
	anitimer.wait_time = time
	anitimer.one_shot = true
	call_deferred("add_child",anitimer)
	anitimer.start()
	if !stop_shaking:
		anitimer.connect("timeout",self,"shake")
	else:
		target = Vector2(0.0,0.0)
	anitw = Tween.new()
	anitw.interpolate_property(self,"position",position,target,time,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	call_deferred("add_child",anitw)
	anitw.start()

func stopshake():
	print("trying to stop!!")
	stop_shaking = true
	if shaketimer!=null:
		shaketimer.queue_free()
	shaketimer=null
	print("exited stopshaker!!")


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
